﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgrammingWarmup2_mcronin : MonoBehaviour
{

    public float Speed = 10;

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
        Vector3 heading = new Vector3(x, 0, z);
        rb.velocity = heading * Speed;
        
    }
}


    
