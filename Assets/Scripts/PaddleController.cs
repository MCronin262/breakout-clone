﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleController : MonoBehaviour {

    public AudioSource music;
    public float Speed = 20;
    public GameObject ballSlot;
    public float timeActual = 15;
    private float timer;
    private bool crown = false;
    public AudioSource winner;
    private int noiseMaker = 0;

    
    void Start () {
        music.Play();
        timer = timeActual;
	}
	
	void Update () {

        BallController ball = ballSlot.gameObject.GetComponent<BallController>();

        float x = Input.GetAxis("Horizontal");
        Vector3 heading = new Vector3(x, 0, 0);
        Vector3 velocity = heading * Speed;
        transform.position = transform.position + velocity * Time.deltaTime;

        if (crown == true)
        {
            timer -= Time.deltaTime;
   
           transform.localScale = new Vector3(1.5F, 1F, 1f);
           
            if (timer < 0)
            {
       
                transform.localScale = new Vector3(1F, 1F, 1);
                crown = false;
                timer = timeActual;
            }
        }

        if ((ball.bricks==0) && (ball.levelNext == true))
        { if (noiseMaker == 0) { music.Stop(); winner.Play(); noiseMaker = 1; } }
    }

    void OnCollisionEnter(Collision c)
    {
        BallController ball = ballSlot.gameObject.GetComponent<BallController>();
   
        if (c.gameObject.tag == "pUp")
        {
            c.gameObject.SetActive(false);
            

            MeshRenderer r = ball.GetComponentInChildren<MeshRenderer>();

            r.material.color = Random.ColorHSV(.2f, 1f, 1f, 1f, 0.5f, 1f);
            ball.pickUpSound.Play();
            //Debug.Log("Green");
        }
        else if (c.gameObject.tag == "pUp2")
        {
            c.gameObject.SetActive(false);            
            ball.argon = ball.argon - 5;
            ball.pickUpSound.Play();
            //Debug.Log("Red");
        }
        else if (c.gameObject.tag == "pUp1")
        {
            c.gameObject.SetActive(false);            
            crown = true;
            ball.pickUpSound.Play();
            //Debug.Log("Yellow");
        }
    }
}
