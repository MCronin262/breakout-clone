﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeathZoneScript : MonoBehaviour
{

    public int lives = 3;
    public Text livesText;
    public AudioSource lifeLost;
    public AudioSource gameLost;
    private int noiseMaker = 0;
    public ParticleSystem deathPoof;



    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "pUp") { other.gameObject.SetActive(false); }
        else if (other.gameObject.tag == "pUp1") { other.gameObject.SetActive(false); }
        else if (other.gameObject.tag == "pUp2") { other.gameObject.SetActive(false); }
        else
        {
            lives -= 1;
            lifeLost.Play();
            lifeLost.pitch = Random.Range(0.9f, 1.1f);
            deathPoof.Play();
        }

    }


        
        void Update()
        {
        
        if (lives > 0)
        {
            livesText.text = "Lives: " + lives;
        }
        else
        { livesText.text = "You Lose!";
            if (noiseMaker == 0)
            {
                gameLost.Play(); ; noiseMaker = 1;
            }
                gameLost.pitch = Random.Range(0.1f, 0.2f);
            
            }
          
        }
}

