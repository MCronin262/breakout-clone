﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallController : MonoBehaviour
{
    public int bricks = 45;
    public int argon = 0;
    public bool trip = false;
    public Text brickText;
    public Text scoreText;
    public Rigidbody rb;
    public AudioSource audioBreak;
    public AudioSource notSound;
    public ParticleSystem notPoof;
    public Transform paddle;
    public AudioSource gameWon;
    private bool spaceLock = false;
    private int lives2 = 3;
    public float speed = 20;    
    public ParticleSystem poof;
    public AudioSource gameStart;
    public GameObject[] powerupPrefabs;
    public ParticleSystem scorePoof;
    int score2 = 0;
    int top = 0;
    public GameObject flame;
    public GameObject bbrickSpawner;
    public bool levelNext=false;
    public AudioSource pickUpSound;
    public float squash = 0.1f;
    private float boing;

    void Start()
    {
        transform.parent = paddle;
        transform.position = paddle.position + new Vector3(0, 5, 0);
         top = PlayerPrefs.GetInt("highScore", 0);
    }
        
    void OnCollisionExit(Collision c)
    {
        BrickController brick = c.gameObject.GetComponent<BrickController>();

        if (brick != null)
        {
            bricks--;
            audioBreak.Play();
            audioBreak.pitch = Random.Range(0.9f, 1.1f);
            argon=argon+10;
            score2 = score2 + 10;
            poof.transform.position = brick.transform.position;
            poof.Play();
            scorePoof.transform.position = brick.transform.position;
            scorePoof.Play();

            if (score2 > top) { PlayerPrefs.SetInt("highScore", score2); }

            if (Random.Range(1, 15) == 1) { GameObject powerup = Instantiate(powerupPrefabs[Random.Range(0, powerupPrefabs.Length)]); powerup.transform.position = brick.transform.position; }

        }
        else if (c.gameObject.tag == "Player")
        {
            notSound.Play();
            notSound.pitch = Random.Range(0.9f, 1.1f);
            notPoof.Play();
        }
        else 
        {
            notSound.Play();
            notSound.pitch = Random.Range(0.8f, 1.0f);
        }
    }

    void OnCollisionEnter(Collision c)
    {
        if (c.contacts.Length > 0)
        {

            boing = Time.time;
            transform.up = c.contacts[0].normal;
            transform.localScale = new Vector3(1.1f, 0.8f, 1.1f);
        }

        if (c.gameObject.GetComponentInParent<PaddleController>())
        {
            float x = transform.position.x - c.gameObject.transform.position.x;
            rb.velocity = new Vector3(x, 1, 0).normalized * speed;
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        trip = true;
    }

    void Update()
    {

        BrickSpawner bSpawn = bbrickSpawner.gameObject.GetComponent<BrickSpawner>();

        if (spaceLock)
        {
            Vector3 v = rb.velocity;
            float dot = Vector3.Dot(v.normalized, Vector3.right);
            float abs = Mathf.Abs(dot);
            if (abs > 0.9f) { v.y *= 1.1f; }
            rb.velocity = v.normalized * speed;
        }

        if (spaceLock == false && Input.GetButtonDown("Jump"))
        {
            rb.velocity = Vector3.up.normalized * speed;
            spaceLock = true;
            transform.parent = null;
            gameStart.Play();

        }
        else if (trip == true)
        {
            lives2--;
            if (lives2 > 0)
            {
                rb.velocity = new Vector3(0, 0, 0);
                transform.localScale = new Vector3(1, 1, 1);
                transform.parent = paddle;
                transform.position = paddle.position + new Vector3(0, 5, 0);
                spaceLock = false;
                trip = false;
            }

        }


        if (bricks > 0)
        {
            scoreText.text = argon + " ";
            brickText.text = " ";
        }
        else if ((bSpawn.nextLevel == true)& levelNext==false)
        {

            if (lives2 > 0)
            {
                bricks = 54;
                rb.velocity = new Vector3(0, 0, 0);
                transform.localScale = new Vector3(1, 1, 1);
                transform.parent = paddle;
                
                transform.position = paddle.position + new Vector3(0, 5, 0);
                spaceLock = false;
                trip = false;
            }
            levelNext = true;
            if (spaceLock == false && Input.GetButtonDown("Jump"))
            {
                
                rb.velocity = Vector3.up.normalized * speed;
                spaceLock = true;
                transform.parent = null;
                gameStart.Play();

            }

        }
        else
        {
            brickText.text = "You Win!";
            scoreText.text = argon + " ";
            
                  gameObject.SetActive(false);
            

            
        }

        
        if (spaceLock==true)
        {
            Vector3 v = rb.velocity;
            if (v.magnitude < 0.1f)
            {
                v = Vector3.up * speed;
            }
            if (Mathf.Abs(Vector3.Dot(v.normalized, Vector3.right)) > 0.9f)
            {
                v.y *= 1.1f;
            }
            rb.velocity = v.normalized * speed;

            if (Time.time - boing > squash)
            {
                transform.up = v.normalized;
                transform.localScale = new Vector3(0.9f, 1.1f, 0.9f);
            }
        }
        else
        {
            rb.velocity = Vector3.zero;
            transform.localScale = Vector3.one;
        }

    }
}