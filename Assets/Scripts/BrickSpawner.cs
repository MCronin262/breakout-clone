﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickSpawner : MonoBehaviour {
    public GameObject brickPrefab;
    public int rows = 5;
        public int cols = 9;
    private GameObject[] bricks;
    public GameObject ballObject;
    
    public bool nextLevel = false;



	// Use this for initialization
	void Start () {

        

        bricks = new GameObject[rows*cols];
        for (int x = 0; x < cols; x++) {
            for (int y = 0; y < rows; y++) { 
                GameObject brick = Instantiate(brickPrefab);
                
                brick.transform.position = new Vector3((x - cols / 2) * 6, (y + rows * 1) * 4, 0);
                bricks[y * cols + x] = brick;
                MeshRenderer r = brick.GetComponentInChildren<MeshRenderer>();
                
                r.material.color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
            }
            
        }
    }

    void OnCollisionEnter()
    {

        gameObject.SetActive(false);

    }
    void Update()
    {
        BallController bNumber = ballObject.GetComponent<BallController>();
        if (bNumber.bricks==0 && nextLevel==false) {
            rows = 6;
            cols = 9;
        bricks = new GameObject[rows * cols];
            for (int x = 0; x < cols; x++)
            {
                for (int y = 0; y < rows; y++)
                {
                    GameObject brick = Instantiate(brickPrefab);
    
                    brick.transform.position = new Vector3((x - (cols) / 2) * 6, (y + (rows )* 1) * 3, 0);
                    bricks[y * cols + x] = brick;
                    MeshRenderer r = brick.GetComponentInChildren<MeshRenderer>();
    
                    r.material.color = Random.ColorHSV(.3f, 1f, 1f, 1f, 0.5f, 1f);
                }
    
            }
            nextLevel = true;
        }
        
    }
} 




    
	
	
